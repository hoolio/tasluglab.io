Title: Hobart TasPubLUG: Thursday 17 May - Brunswick Hotel
Date: 2018-05-19
Category: Hobart
Tags: taspublug

The third Thursday of the month is fast approaching which means it is time to
meet up and chat about all things related to free and open source software,
hardware, and culture.

This month we will be heading to previous TasLUG haunt... the Brunswick Hotel.

When:
: 6:30pm, Thursday 17 May 2018

Where:
: Brunswick Hotel (67 Liverpool Street, Hobart)

Please let John know if you are planning to come along, even if just for a
quick drink. If we hear from more than a handful of people by Tuesday that they
are coming then we'll book a table.

It was great to see a good turnout last month, so we can repeat that this
month.

Title: Launceston Meeting: 26th May - Installing Debian
Date: 2018-05-21 12:03 +1000
Category: Launceston

Hi people! For this month's Launceston meeting, we'll be going through the
steps of installing the [Debian](http://www.debian.org/) GNU/Linux operating
system, and then take a look at its features and default software.

Where:
: Enterprize<br />
  22-24 Patterson St

When:
: Saturday 26th May<br />
  2:00pm Start

This meeting's presentation may be focused toward newcomers, but as always,
everybody is welcome to come along and join the discussion. Hope to see you
there!

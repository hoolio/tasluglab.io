Title: Launceston Meeting: 30th June - Painting With GIMP 2.10
Date: 2018-06-22 21:58 +1000
Category: Launceston

Hi people! For this month's Launceston meeting, we will look the [2.10
release](https://www.gimp.org/release-notes/gimp-2.10.html) of the GNU Image
Manipulation Program (GIMP). We'll also be doing some live painting and image
editing to show off GIMP's new symmetry and non-destructive canvass rotation
features.

Where:
: Enterprize<br />
  22-24 Patterson St

When:
: Saturday 30th June<br />
  2:00pm Start

As always, everybody is welcome to come along and join the discussion. Hope to
see you there!

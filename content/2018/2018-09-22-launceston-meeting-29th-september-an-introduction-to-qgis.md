Title: Launceston Meeting: 29th September - An introduction to QGIS
Date: 2018-09-22 06:30 +1000
Category: Launceston

Hi people! For this month's Launceston meeting, we will be taking a look at the
F/OSS desktop Geographic Information System,
[QGIS](https://www.qgis.org/en/site/), which supports viewing, editing and
analysis of geospatial data. In this talk we will demonstrate how to connect to
a variety of different data sources for displaying within a map, as well as
adding in new data and editing existing data.

Where:
: Enterprize<br />
  22-24 Patterson St

When:
: Saturday 29th September<br />
  2:00pm Start

As always, everybody is welcome to come along and join the discussion. Hope to
see you there!

Title: Hobart TasLUG Meeting: Thursday 21 February
Date: 2019-02-14
Category: Hobart
Tags: meeting

The holiday season is well a truly behind us and it is now time for the first
Hobart TasLUG meeting of the year. This month's meeting will feature two talks:

---

#### Scott Bragg
LoRaWAN IoT - The hardware components you can add to your microcontrollers to
join TheThingsNetwork (TTN), how LoRaWAN works and collecting data from your
devices.

#### Ian Stevenson
Simple backups for home servers and workstations.

---

The venue for this month will be Enterprize Hobart:

When:
: Thursday 21 February 2019 - 6:00pm for a 6:30pm start

Where:
: Enterprize - Hobart<br />
  5th floor, Hobart City Council Building<br />
  24 Davey Street ([map](https://goo.gl/maps/SNWs8FJcvEk))<br />

**Note:** the doors inside the foyer will be locked, so you will need to use the
intercom to let us know to buzz you in. However, if you arrive after 6:30pm,
then we'll leave a note with a phone number to call as the intercom isn't near
where the talks will be.

Enterprize provide a selection of alcoholic/non-alcoholic drinks and coffee
pods for the appropriate donation, as well as free instant coffee and tea. So
come at 6:00pm, get a drink, and have a bit of a mingle before the talks start
at 6:30pm. We will also be heading out to find somewhere to eat afterwards for
those interested.

I would like to to extend a big thank you to
[Enterprize](https://www.enterprize.space/) for generously allowing
us to use their space to hold regular meetings. 

Title: Hobart TasLUG meeting day survey
Date: 2019-02-27
Category: Hobart

There was recently a query about the possibility of changing the day of the
month that the Hobart TasLUG meetings are held. Given this it seemed like it
would be a good opportunity to do a general survey of when people are available
to attend meetings.

With this in mind a [survey has been set up for people to fill
out](https://framadate.org/hobart-taslug). For each day of the month you can
select whether you can make it to meetings (green tick), can't make it on that
day (red cross), or would be a maybe (amber tick in parens).

It would be great if everyone interested in attending Hobart meetings could
enter their preferences, especially if the current meetings day makes it
difficult to attend.

Please note that this survey is for informational purposes only and will only
be one factor taken into consideration when determining the best meeting day
going forward. 

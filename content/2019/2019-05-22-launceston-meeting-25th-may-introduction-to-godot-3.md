Title: Launceston May Meeting - An introduction to making games with Godot 3
Date: 2019-05-22
Category: Launceston

Hi people. Apologies for the super late announcement! For this month's Launceston meeting, Cheese will be talking about the [Godot game engine](http://godotengine.org/) and his recent experiences with making the upcoming management sim/base building game [Hive Time](http://hivetime.twolofbees.com/) with it. We'll be looking at general engine usage, some patterns for input handlers, and workflows for getting 2D and 3D resources into the engine.

Want to make fancy looking games, but don't know where to get started? Get some tips at this month's meeting!

Where:
: Enterprize<br />
  22-24 Patterson St

When:
: Saturday 25th May<br />
  2:00pm Start

As always, everybody is welcome to come along and join the discussion. Hope to see you there!

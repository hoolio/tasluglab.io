Title: No Launceston August Meeting
Date: 2019-08-05
Category: Launceston

Hi people! Enterprize in Launceston is in the process of relocating to Macquarie House. To help avoid being underfoot and to give people notice of the new venue, we've decided to skip this month's meeting.

We look forward to seeing you in September in Enterprize's new venue!

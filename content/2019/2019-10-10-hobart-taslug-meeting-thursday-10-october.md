Title: Hobart TasLUG Meeting: Thursday 10 October
Date: 2019-10-04
Author: John Kristensen
Category: Hobart
Tags: meeting

This months TasLUG meeting is next week and will feature the following talks:

---

#### Simon Dwyer
[ZeroTier](https://www.zerotier.com/) is not a VPN.... ZeroTier is a overlay
network.  Not know the difference?  Nether did Simon.  Come along and hear how
this overlay network technology could change the way you communicate with your
infrastructure on daily basis.

#### John Kristensen
[Kubernetes](https://kubernetes.io/) is a container-orchestration system for
automating application deployment, scaling, and management. John will give a
very simple overview of the main Kubernetes components and what they do when an
application is deployed.

---

The venue for this month will be Enterprize Hobart:

When:
: Thursday 10 October 2019 - 6:00pm for a 6:30pm start

Where:
: Enterprize - Hobart<br />
  5th floor, Hobart City Council Building<br />
  24 Davey Street ([map](https://goo.gl/maps/SNWs8FJcvEk))<br />

***Note:** the doors inside the foyer will be locked, so you will need to use
the intercom to let us know to buzz you in.*

Enterprize provide a selection of alcoholic/non-alcoholic drinks and coffee
pods for the appropriate donation, as well as free instant coffee and tea. So
arrive at 6:00pm, get a drink, and have a bit of a mingle before the talks
start at 6:30pm. We will also be heading out to find somewhere to eat
afterwards for those interested.

We would like to to extend a big thank you to
[Enterprize](https://www.enterprize.space/) for generously allowing us to use
their space to hold regular meetings.

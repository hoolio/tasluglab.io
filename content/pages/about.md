Title: About
SortOrder: 10

The Tasmanian Linux Users Group (TasLUG) promotes the use of Linux as well as
Free, Libre, and Open Source Software (FLOSS) in general. 

We have monthly [meetings]({filename}./meetings.md) in both the North and South
of the state.

We are a friendly group of hackers and makers that use open source and open
hardware in our work and play. Our monthly meetings provide a focus of what
each of us are currently working on and learning about. We also chat frequently
online with our [Matrix/IRC chat room]({filename}./chat.md) and
[mailing list]({filename}./mailing-list.md).

TasLUG is a subcommittee of [Linux Australia](http://linux.org.au).

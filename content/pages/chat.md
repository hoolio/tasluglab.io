Title: Matrix/IRC Chat
Slug: chat
SortOrder: 30

A number of TasLUG members can regularly be found hanging out and chatting on
Matrix/IRC. If you are unable to make it to meetings, then joining the chat
room is an easy way to still be part of the community.


## Matrix
[Matrix](https://matrix.org/) is a federated chat protocol that runs over HTTP
and provides support for writing bridges and bots to interact with other
systems. Quite a few of the TasLUG members like it because:

  * It is a federated system which allows self-host servers and bridges.
  * It supports end-to-end encryption (in private chats and some rooms).
  * Has VoIP voice and video capabilities.
  * Can share images and links.
  * Bridges to FreeNode and other IRC networks.
  * Bridges to other chat platforms.
  * Has a number of different
    [clients](https://matrix.org/docs/projects/clients-matrix) for various
    platforms.

The [Riot](https://riot.im/) web client makes it very easy to join the
[TasLUG Matrix room](https://riot.im/app/#/room/#taslug:matrix.org) using your
browser.

There are also mobile clients available in the app store of your device (search
for 'Riot IM').


## IRC
If members prefer the venerable IRC, then TasLUG also has a long running IRC
channel (`#taslug`) hosted on the [Freenode](https://freenode.net/) network.
The TasLUG IRC channel is bridged with the Matrix TasLUG room so members can
use their favourite IRC client to chat with everyone:

  * [weechat](https://weechat.org/)
  * [irssi](https://irssi.org/)
  * [xchat](http://xchat.org/)

Connect to `chat.freenode.net` and join `#taslug`.
